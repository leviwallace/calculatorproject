var canv = document.getElementById("myCanvas");
var ctx = canv.getContext("2d");
// wind = window / domain and range of screen
var wind = {x: {min: -10, max: 10}, y: {min: -10, max: 10}};
var scale = 0;
var debugList = [new Quadratic("x*x", "red"), new Quadratic("x*x*x", "blue"), new Quadratic("-x*x*x+10", "purple")];
var flist = [];
var Fl;
var Color;
var Type = 0;
var masterColors = new Set(["red", "blue", "green", "yellow", "pink", "brown", "black", "orange", "white", "magenta", "purple"])

function setType(t)
{
  Type = t;
}

// Mapinterval [x1, x3], [y1, y2]
// take ratio defined in first interval then map to second interval

function mapInterval(x1, x2, x3, y1, y2)
{
  // x1 = 2, x2 = 5, x3 = 7
  xrange = (x3 - x1)
  yrange = (y2 - y1)
  percentofRange = (x2 - x1)/xrange
  alongY = percentofRange * yrange
  return y1+alongY;
}

function background()
{
  ctx.beginPath();
  ctx.rect(0, 0, canv.width, canv.height);
  ctx.fillStyle = "#e5e3d5";
  ctx.fill();
}

function drawAxis()
{
  ctx.strokeStyle = "#000000"
  px = mapInterval(wind.x.min, 0, wind.x.max, 0, canv.width);
  py = mapInterval(wind.y.min, 0, wind.y.max, canv.height, 0);
  ctx.beginPath();
  ctx.moveTo(0, py);
  ctx.lineTo(canv.width, py);
  ctx.moveTo(px, 0);
  ctx.lineTo(px, canv.height);
  ctx.stroke();
}

function drawLines()
{
  ctx.strokeStyle = "#c4c4c4";
  var dx = Math.round(wind.x.min);
  while(dx <= wind.x.max)
  {
    tx = mapInterval(wind.x.min, dx, wind.x.max, 0, canv.width)
    ctx.beginPath();
    ctx.moveTo(tx, 0);
    ctx.lineTo(tx, canv.height);
    ctx.stroke();
    dx += 1;
  }
  var dy = Math.round(wind.y.min);
  while(dy <= wind.y.max)
  {
    var ty = mapInterval(wind.y.min, dy, wind.y.max, 0, canv.height)
    ctx.beginPath();
    ctx.moveTo(0, ty);
    ctx.lineTo(canv.width, ty);
    ctx.stroke();
    dy++;
  }
}

function setScale()
{
  var sxmin = Number(document.getElementById("sx-min").value);
  var sxmax = Number(document.getElementById("sx-max").value);
  var symin = Number(document.getElementById("sy-min").value);
  var symax = Number(document.getElementById("sy-max").value);
  wind = {x: {min: sxmin, max: sxmax}, y: {min: symin, max: symax}};
  refresh();
}


function f(x, c)
{
  return eval(c);
}

function drawPoint(x, y, c)
{
  ctx.beginPath();
  ctx.fillStyle = c;
  ctx.arc(x, y, 3, 0, 2 * Math.PI, false);
  ctx.fill();
}

function drawLine(x1, y1, x2, y2, c)
{
  ctx.lineWidth = 3;
  ctx.strokeStyle = c;
  ctx.beginPath();
  ctx.moveTo(x1, y1);
  ctx.lineTo(x2, y2);
  ctx.stroke();
}

function newFunction()
{
  Fl = document.getElementById("fl").value;
  var temp = new Set(masterColors);
  flist.forEach(function (f)
  {
    temp.delete(f.color)
  })
  var colorToUse = null;
  for (c of temp) {
   colorToUse = c;
   break;
  }
  if(Type == 0) {
    flist.push(new Quadratic(Fl, colorToUse));
  }
  if(Type == 1) {
    flist.push(new Parametric(5, colorToUse));
  }
  refresh();
}

function removeFunctionIndex(i)
{
    flist.splice(i, 1);
    refresh();
}

function displayFunctions()
{
  var functionList = document.getElementById("function-list");
  while(functionList.firstChild)
  {
    functionList.removeChild(functionList.firstChild);
  }
  for(var i = 0; i < flist.length; i++)
  {
    var f = flist[i];
    var d = document.createElement("div");
    var s1 = document.createElement("span");
    var s2 = document.createElement("span");
    var s3 = document.createElement("span");
    s1.innerHTML = f.fofx;
    s2.setAttribute("style", "background: " + f.color);
    var b = document.createElement("input");
    b.setAttribute("type", "button");
    b.setAttribute("onclick", "removeFunctionIndex(" + i +")");
    b.setAttribute("value", "Delete Function")
    s3.appendChild(b);
    d.appendChild(s1);
    d.appendChild(s2);
    d.appendChild(s3);
    d.classList.add("fline");
    functionList.appendChild(d);
  };
}

function plotFunctions()
{
    flist.forEach(function(entry)
    {
      entry.draw();
    });
}

function refresh()
{
  ctx.clearRect(0,0, canv.width, canv.height);
  background();
  drawLines();
  drawAxis();
  plotFunctions();
  document.getElementById("sx-min").value = wind.x.min;
  document.getElementById("sx-max").value = wind.x.max;
  document.getElementById("sy-min").value = wind.y.min;
  document.getElementById("sy-max").value = wind.y.max;
  displayFunctions();
  colorSet = new Set([]);
  colorSet.add("green");
  console.log(colorSet);
}

refresh();
