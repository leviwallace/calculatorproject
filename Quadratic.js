Quadratic = function(s, color)
{
  this.fofx = s;
  this.color = color;
  this.draw = function()
  {
    var x;
    var y;
    var previous;
    for(let px = 0; px < canv.width; px++)
    {
      // px = in screen
      x = mapInterval(0, px, canv.width, wind.x.min, wind.x.max);
      // domain of x
      function asin()
      {
        return "Math.asin";
      }
      function acos()
      {
        return "Math.acos";
      }
      function atan()
      {
        return "Math.atan";
      }
      var s2 = this.fofx

      // .toLowerCase().replace("arcsin", asin()).replace("arccos", acos())
      // .replace("arctan", atan()).replace("sin", "Math.sin")
      // .replace("cos", "Math.cos").replace("tan", "Math.tan")
      // .replace("floor", "Math.floor").replace("random", "Math.random")


      y = eval(s2);
      // pop out of domain back to screen
      // [wind.y.min, y ,windm.y.max]
      // [0, canv.height]
      py = mapInterval(wind.y.min, y, wind.y.max, canv.height, 0);
      if (px !== 0)
      {
        drawLine(px, py, previous.x2, previous.y2, this.color);
      }
      previous = {x2: px, y2: py};
    }
  }
}
