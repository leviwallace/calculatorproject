Parametric = function(r, color)
{
  this.r = r;
  this.color = color;

  this.draw = function()
  {
    var x;
    var y;
    var previous;
    for(let i = 0; i < 360; i++)
    {
      let theta = i * 2 * Math.PI/360;
      x = Math.cos(theta) * this.r;
      y = Math.sin(theta) * this.r;
      // px = mapInterval(0, x, canv.width, wind.x.max, wind.x.min);
      // py = mapInterval(0, y, canv.height, wind.y.max, wind.y.min)
      if (i !== 0)
      {
        drawLine(x, y, previous.x2, previous.y2, this.color);
      }
      previous = {x2: x, y2: y};
    }
  }
}
